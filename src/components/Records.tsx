import React, { useState, useEffect } from 'react';
import axios from 'axios';

const Records = () => {
	const [records, setRecords] = useState([]);

	useEffect(() => {
		axios
			.get('http://localhost:4000/records')
			.then((response) => {
				setRecords(response.data);
			})
			.catch((error) => {
				console.log(error);
			});
	}, []);

	return (
		<div className="records">
			<h3 className="records__title">Best Records</h3>
			<div className="records__list">
				{records
					? records.map((record: any, index) => (
							<h6 className="records__list__item" key={index}>
								{index + 1}. {record.first_name} {record.last_name} with{' '}
								{record.moves} moves in {record.time}
							</h6>
					  ))
					: null}
			</div>
		</div>
	);
};

export default Records;
