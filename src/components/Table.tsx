import React, { useState, useEffect } from 'react';
import Modal from 'react-modal';
import axios from 'axios';
import { produce } from 'immer';

import Timer from './Timer';
import Records from './Records';
import { TimeContext } from '../context/TimeContext';
import { COLOR } from '../constants/COLOR';

Modal.setAppElement('#root');

const customStyles = {
	overlay: {
		backgroundColor: 'transparent',
	},
	content: {
		backgroundColor: COLOR.dark_grey,
		width: '80rem',
		height: '45rem',
		top: '50%',
		left: '50%',
		padding: '0',
		transform: 'translate(-50%, -50%)',
		transition: 'all 5s',
	},
};

const createGrid = () => {
	const rows: number[][] = [];
	for (let i = 0; i < 5; i++) {
		/* alternative:
		let tempArr = [];
		for (let j = 0; j < 5; j++) {
			tempArr.push(0);
		}
		rows.push(tempArr);
		*/
		rows.push(Array.from(Array(5), () => (Math.random() > 0.6 ? 0 : 1)));
	}
	return rows;
};

const Table = () => {
	const [grid, setGrid] = useState(() => createGrid());
	const [numOfMove, setNumOfMove] = useState(0);
	const [isactive, setIsActive] = useState(false);
	const [reset, setReset] = useState(false);
	const [time, setTime] = useState('');
	const [showFormActive, setShowFormActive] = useState(false);
	const [fname, setFName] = useState('');
	const [lname, setLName] = useState('');

	const showForm = () => {
		return (
			<Modal
				isOpen={showFormActive}
				onRequestClose={() => setShowFormActive(false)}
				style={customStyles}
			>
				<span onClick={() => setShowFormActive(false)} className="material-icons close">
					close
				</span>
				<div className="content">
					<h2 className="content__title">hey congratulations</h2>
					<form className="form" onSubmit={(event) => event.preventDefault()}>
						<div className="form__group">
							<input
								value={fname}
								onChange={(event) => setFName(event.target.value)}
								type="text"
								className="form__input"
								placeholder="First Name"
								id="fname"
								required
							/>
							<label htmlFor="fname" className="form__label">
								First Name
							</label>
						</div>
						<div className="form__group">
							<input
								value={lname}
								onChange={(event) => setLName(event.target.value)}
								type="text"
								className="form__input"
								placeholder="Last Name"
								id="lname"
								required
							/>
							<label htmlFor="lname" className="form__label">
								First Name
							</label>
						</div>
						<div className="form__buttonContainer">
							<button
								className="form__buttonContainer__btn"
								onClick={() => setShowFormActive(false)}
							>
								Cancel
							</button>
							<button
								className="form__buttonContainer__btn"
								onClick={() => {
									axios
										.post('http://localhost:4000/new_record', {
											first_name: fname,
											last_name: lname,
											time: time,
											moves: numOfMove,
										})
										.then((response) => {
											console.log(response.status);
											if (response.status === 200) {
												setShowFormActive(false);
												setGrid(createGrid());
												setNumOfMove(0);
												setReset(true);
											}
										})
										.catch((error) => {
											console.log(error);
										});
								}}
							>
								Submit
							</button>
						</div>
					</form>
				</div>
			</Modal>
		);
	};

	// check if we are in the end game!
	useEffect(() => {
		let gameEnd = true;
		for (let i = 0; i < 5; i++) {
			for (let j = 0; j < 5; j++) {
				if (grid[i][j]) {
					gameEnd = false;
				}
			}
		}
		if (gameEnd) {
			setIsActive(false);
			setShowFormActive(true);
		} else {
			setShowFormActive(false);
		}
	}, [grid]);

	const drawTable = () => {
		return grid.map((row, i) =>
			row.map((col, j) => (
				<div
					key={`${i}-${j}`}
					onClick={() => {
						setIsActive(true);
						// immutable state
						setNumOfMove((num) => (num += 1));
						const newGrid = produce(grid, (gridCopy) => {
							gridCopy[i][j] = gridCopy[i][j] ? 0 : 1;
							if (i !== 0) {
								gridCopy[i - 1][j] = gridCopy[i - 1][j] ? 0 : 1;
							}
							if (i !== 4) {
								gridCopy[i + 1][j] = gridCopy[i + 1][j] ? 0 : 1;
							}
							if (j !== 0) {
								gridCopy[i][j - 1] = gridCopy[i][j - 1] ? 0 : 1;
							}
							if (j !== 4) {
								gridCopy[i][j + 1] = gridCopy[i][j + 1] ? 0 : 1;
							}
						});
						setGrid(newGrid);
					}}
					style={{
						backgroundColor: grid[i][j] ? COLOR.primary : COLOR.block_background,
						transition: 'all .3s',
					}}
					className="table__block"
				></div>
			))
		);
	};

	const resetHandler = () => {
		setReset(false);
		setIsActive(false);
	};

	return (
		<TimeContext.Provider value={{ time, setTime }}>
			{showFormActive ? showForm() : null}
			<div className="title">
				<h2 className="title__text">Lights Out</h2>
			</div>
			<div className="table">
				<div className="table__config">
					<div
						className="table__config__container"
						onClick={() => {
							setGrid(createGrid());
							setNumOfMove(0);
							setReset(true);
						}}
					>
						<span className="material-icons table__config__container__icon">
							autorenew
						</span>
						<h6>New Game</h6>
					</div>
					<div
						className="table__config__container"
						onClick={() => {
							setIsActive(!isactive);
						}}
					>
						<span className="material-icons table__config__container__icon">pause</span>
						<h6>Pause</h6>
					</div>
				</div>
				{drawTable()}
			</div>
			<Records />

			<div className="recordContainer">
				<h4>Moves: {numOfMove}</h4>
				<Timer active={isactive} reset={reset} resetHandler={resetHandler} />
			</div>
		</TimeContext.Provider>
	);
};

export default Table;
