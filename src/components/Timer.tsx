import React, { useState, useEffect, useContext } from 'react';

import { TimeContext } from '../context/TimeContext';

interface ITimerActiveProps {
	active: boolean;
	reset: boolean;
	resetHandler: () => void;
}

const Timer = (props: ITimerActiveProps) => {
	const [seconds, setSeconds] = useState(0);
	const [minutes, setMinutes] = useState(0);
	const { setTime } = useContext(TimeContext);

	useEffect(() => {
		let interval: number | any;

		if (props.reset) {
			setSeconds(0);
			setMinutes(0);
			props.resetHandler();
		}

		if (props.active) {
			interval = setInterval(() => {
				setSeconds((time) => time + 1);
			}, 1000);

			if (seconds > 59) {
				setSeconds(0);
				setMinutes((minute) => minute + 1);
			}
		}

		setTime(`00:${minutes < 10 ? 0 : null}${minutes}:${seconds < 10 ? 0 : ''}${seconds}`);

		return () => clearInterval(interval);
	}, [seconds, minutes, props.active, props.reset]);

	return (
		<h4>
			Time:
			<span className="timer">
				{minutes < 10 ? 0 : null}
				{minutes}:{seconds < 10 ? 0 : null}
				{seconds}
			</span>
			<br />
		</h4>
	);
};

export default Timer;
