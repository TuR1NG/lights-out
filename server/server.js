const express = require('express');
const bodyParser = require('body-parser');

const cors = require('cors');
const db = require('./helper/database');

const app = express();

const port = process.env.PORT || 4000;

app.use(bodyParser());

app.use(function (req, res, next) {
	res.header('Access-Control-Allow-Origin', req.headers.origin);
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
	next();
});

app.use(
	cors({
		origin: /req.origin/,
		credentials: true,
		preflightContinue: true,
	})
);

app.post('/new_record', async (req, res) => {
	console.log(req.body);
	await db.execute(
		'INSERT INTO records (first_name, last_name, moves, time) VALUES (?, ?, ?, ?)',
		[req.body.first_name, req.body.last_name, req.body.moves, req.body.time]
	);
	return res.status(200).send();
});

app.get('/records', async (req, res) => {
	const recordsList = await db.execute('SELECT * FROM records ORDER BY time ASC limit 10');
	return res.status(200).send(recordsList[0]);
});

app.listen(port, () => {
	console.log(`Server is up on ${port}!`);
});
