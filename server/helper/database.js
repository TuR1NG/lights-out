const mysql = require('mysql2');

const pool = mysql.createPool({
	host: 'localhost',
	user: 'root',
	database: 'lights_out',
	password: 'm.a.a.p.2000',
});

module.exports = pool.promise();
